@extends('master')
@section('judul')
    Detail {{$pemainbaru->nama}}
@endsection

@section('content')

<h1>{{$pemainbaru->nama}}</h1>
<p>Umur : {{$pemainbaru->umur}}</p>
<p>Bio : {{$pemainbaru->bio}}</p>

@endsection