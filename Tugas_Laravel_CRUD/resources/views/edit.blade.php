@extends('master')
@section('judul')
    Edit data pemail film {{$pemainbaru->nama}}
@endsection

@section('content')
    <form action="/cast/{{$pemainbaru->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Nama pemain film baru</label>
            <input type="text" class="form-control" value="{{$pemainbaru->nama}}" name="nama" placeholder="Masukkan nama pemain film baru">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Usia pemain film baru</label>
            <input type="text" class="form-control" value="{{$pemainbaru->umur}}" name="umur" placeholder="Masukkan usia pemain film baru">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Bio pemain film baru</label>
            <textarea name="bio" id="" class="form-control" cols="30" rows="10">{{$pemainbaru->bio}}</textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
@endsection