@extends('master')
@section('judul')
    Tambah pemain film baru
@endsection

@section('content')
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
            <label>Nama pemain film baru</label>
            <input type="text" class="form-control" name="nama" placeholder="Masukkan nama pemain film baru">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Usia pemain film baru</label>
            <input type="text" class="form-control" name="umur" placeholder="Masukkan usia pemain film baru">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Bio pemain film baru</label>
            <textarea name="bio" id="" class="form-control" cols="30" rows="10"></textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
@endsection