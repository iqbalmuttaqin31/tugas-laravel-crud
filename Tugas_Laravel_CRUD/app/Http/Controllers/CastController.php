<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function create(){
        return view('create');
    }
    public function store(Request $request){
        $request->validate([
            'nama' => 'required|max:255',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        DB::table('casts')->insert(
            [
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio']
            
            ]
        );
       
        return redirect('/cast');
    }

    public function index(){
        $pemainbaru = DB::table('casts')->get();
        return view('index', compact('pemainbaru'));
    }
    public function show($id){
        $pemainbaru = DB::table('casts')->where('id',$id)->first();
        return view('show', compact('pemainbaru'));
    }
    public function edit($id){
        $pemainbaru = DB::table('casts')->where('id',$id)->first();
        return view('edit', compact('pemainbaru'));
    }
    public function update(Request $request, $id){
        $pemainbaru = DB::table('casts')->where('id',$id)->first();
        $request->validate([
            'nama' => 'required|max:255',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        DB::table('casts')
              ->where('id', $id)
              ->update(
                  [
                      'nama' => $request['nama'],
                      'umur' => $request['umur'],
                      'bio' => $request['bio']
                  ]);
        return redirect('/cast');
    }
     
    public function destroy($id){
        DB::table('casts')->where('id', '=', $id)->delete();
        return redirect('/cast');
    }
}
