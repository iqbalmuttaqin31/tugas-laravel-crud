<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index_lalu');
});

Route::get('/master', function () {
    return view('master');
});

Route::get('/table', function () {
    return view('table');
});

Route::get('/data-tables', function () {
    return view('data-tables');
});

// CRUD

// cast
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');

// Menampilkan data-data pemain film (cast)
Route::get('/cast', 'CastController@index');

// Menampilkan detail data pemain film berdasarkan id-nya
Route::get('/cast/{cast_id}', 'CastController@show');

// Menuju Form Edit data pemain film
Route::get('/cast/{cast_id}/edit', 'CastController@edit');

// Fungsi update berdasarkan id
Route::put('/cast/{cast_id}', 'CastController@update');

// Fungsi delete berdasarkan id
Route::delete('/cast/{cast_id}', 'CastController@destroy');
